package utils

func IsValueInList(value interface{}, list []interface{}) bool {
	for _, v := range list {
		if v == value {
			return true
		}
	}
	return false
}
