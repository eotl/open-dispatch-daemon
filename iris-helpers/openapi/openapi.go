package openapi

import (
	iris_helpers "codeberg.org/eotl/open-dispatch-daemon/iris-helpers"
	"codeberg.org/eotl/open-dispatch-daemon/iris-helpers/controllers"
	"github.com/go-openapi/spec"
	"github.com/google/uuid"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/context"
	"gorm.io/datatypes"
	"reflect"
	"strings"
	"time"
)

type Object = map[string]interface{}

type API struct {
	PathPrefix  string
	Version     string
	Title       string
	Controllers map[string]controllers.RESTController
	Middlewares []context.Handler
}

func (api *API) Mount(appBase iris.Party) {
	apiBase := appBase.Party("/"+api.PathPrefix, api.Middlewares...)

	apiBase.Get("/spec", func(ctx *context.Context) {
		_, _ = ctx.JSON(api.Spec())
	})

	for path, controller := range api.Controllers {
		controller.Mount(apiBase.Party("/" + path))
	}
}

func (api API) Spec() Object {
	allPaths := map[string]interface{}{}
	for controllerPath, controller := range api.Controllers {
		for path, pathItem := range Paths(&controller, api.PathPrefix+"/"+controllerPath) {
			allPaths[path] = pathItem
		}
		for subControllerPath, subcontroller := range controller.SubControllers {
			for path, pathItem := range Paths(
				&subcontroller,
				api.PathPrefix+"/"+controllerPath+"/"+controller.IdPath()+"/"+subControllerPath,
			) {
				allPaths[path] = pathItem
			}
		}
		allPaths = additionalHandlerPaths(
			controller.AdditionalHandlers,
			allPaths,
			api.PathPrefix+"/"+controllerPath+"/")
		allPaths = additionalHandlerPaths(
			controller.AdditionalInstanceHandlers,
			allPaths,
			api.PathPrefix+"/"+controllerPath+"/"+controller.IdPath()+"/")
	}
	return map[string]interface{}{
		"openapi": "3.0.0",
		"info": Object{
			"version": api.Version,
			"title":   api.Title,
		},
		"paths": allPaths,
		"components": Object{
			"requestBodies": api.requestsMap(),
			"responses":     api.responsesMap(),
			"schemas":       api.schemas(),
		},
	}
}

func additionalHandlerPaths(additionalHandlers map[string]controllers.AdditionalHandlers, allPaths map[string]interface{}, additionalPathsPrefix string) map[string]interface{} {
	for path, additionalHandlers := range additionalHandlers {
		pathParams := []Object{}
		for paramName, paramType := range additionalHandlers.PathParameters {
			pathParams = append(pathParams, Object{
				"in":       "path",
				"name":     paramName,
				"required": true,
				"schema": Object{
					"type": paramType,
				},
			})
		}
		pathItem := Object{}
		for method, additionalHandler := range additionalHandlers.MethodHandlers {
			methodDefinition := Object{
				"parameters": pathParams,
				"responses":  pathResponses(additionalHandler.OutputType.Name()),
			}
			if additionalHandler.InputType != nil {
				methodDefinition["requestBody"] = Object{
					"$ref": "#/components/requestBodies/" + additionalHandler.InputType.Name(),
				}
			}
			pathItem[method] = methodDefinition
		}
		allPaths[additionalPathsPrefix+path] = pathItem
	}
	return allPaths
}

func (api *API) requestsMap() Object {
	requestsMap := Object{}

	for _, controller := range api.Controllers {
		requestsMap[(*controller.ControllerType).Name()] = Object{
			"required": true,
			"content": Object{
				"application/json": Object{
					"schema": Object{
						"$ref": "#/components/schemas/" + (*controller.ControllerType).Name(),
					},
				},
			},
		}
		for _, subcontroller := range controller.SubControllers {
			requestsMap[(*subcontroller.ControllerType).Name()] = Object{
				"required": true,
				"content": Object{
					"application/json": Object{
						"schema": Object{
							"$ref": "#/components/schemas/" + (*subcontroller.ControllerType).Name(),
						},
					},
				},
			}
		}
		for _, additionalHandlers := range controller.AdditionalHandlers {
			for _, methodHandler := range additionalHandlers.MethodHandlers {
				if methodHandler.InputType != nil {
					requestsMap[methodHandler.InputType.Name()] = Object{
						"required": true,
						"content": Object{
							"application/json": Object{
								"schema": Object{
									"$ref": "#/components/schemas/" + methodHandler.InputType.Name(),
								},
							},
						},
					}
				}
			}
		}
		for _, additionalHandlers := range controller.AdditionalInstanceHandlers {
			for _, methodHandler := range additionalHandlers.MethodHandlers {
				if methodHandler.InputType != nil {
					requestsMap[methodHandler.InputType.Name()] = Object{
						"required": true,
						"content": Object{
							"application/json": Object{
								"schema": Object{
									"$ref": "#/components/schemas/" + methodHandler.InputType.Name(),
								},
							},
						},
					}
				}
			}
		}

	}

	return requestsMap
}

func (api *API) responsesMap() Object {
	responsesMap := api.requestsMap()

	for _, controller := range api.Controllers {
		responsesMap[(*controller.ControllerType).Name()+"List"] = Object{
			"content": Object{
				"application/json": Object{
					"schema": Object{
						"tyoe": "array",
						"items": Object{
							"$ref": "#/components/schemas/" + (*controller.ControllerType).Name(),
						},
					},
				},
			},
		}
		for _, subcontroller := range controller.SubControllers {
			responsesMap[(*subcontroller.ControllerType).Name()+"List"] = Object{
				"content": Object{
					"application/json": Object{
						"schema": Object{
							"type": "array",
							"items": Object{
								"$ref": "#/components/schemas/" + (*subcontroller.ControllerType).Name(),
							},
						},
					},
				},
			}
		}
		for _, additionalHandlers := range controller.AdditionalHandlers {
			for _, methodHandler := range additionalHandlers.MethodHandlers {
				responsesMap[methodHandler.OutputType.Name()] = Object{
					"content": Object{
						"application/json": Object{
							"schema": Object{
								"$ref": "#/components/schemas/" + methodHandler.OutputType.Name(),
							},
						},
					},
				}
			}
		}
		for _, additionalHandlers := range controller.AdditionalInstanceHandlers {
			for _, methodHandler := range additionalHandlers.MethodHandlers {
				responsesMap[methodHandler.OutputType.Name()] = Object{
					"content": Object{
						"application/json": Object{
							"schema": Object{
								"$ref": "#/components/schemas/" + methodHandler.OutputType.Name(),
							},
						},
					},
				}
			}
		}

	}

	for _, errorType := range []string{
		"BadRequest",
		"Unauthorized",
		"Forbidden",
		"NotFound",
		"ServerError",
	} {
		responsesMap[errorType] = Object{
			"content": Object{
				"application/json": Object{
					"schema": Object{
						"$ref": "#/components/schemas/ApiError",
					},
				},
			},
		}
	}
	return responsesMap
}

func (api *API) schemas() Object {
	schemaCache := map[reflect.Type]spec.Schema{}

	for _, controller := range api.Controllers {
		StructToSchema(*controller.ControllerType, schemaCache, "/")
		for _, subcontroller := range controller.SubControllers {
			StructToSchema(*subcontroller.ControllerType, schemaCache, "/")
		}
		for _, additionalHandlers := range controller.AdditionalHandlers {
			for _, methodHandler := range additionalHandlers.MethodHandlers {
				if methodHandler.InputType != nil {
					TypeToSchema(methodHandler.InputType, schemaCache, "/")
				}
				TypeToSchema(methodHandler.OutputType, schemaCache, "/")
			}
		}
		for _, additionalHandlers := range controller.AdditionalInstanceHandlers {
			for _, methodHandler := range additionalHandlers.MethodHandlers {
				if methodHandler.InputType != nil {
					TypeToSchema(methodHandler.InputType, schemaCache, "/")
				}
				TypeToSchema(methodHandler.OutputType, schemaCache, "/")
			}
		}
	}

	schemas := Object{}
	for _type, schema := range schemaCache {
		schemas[_type.Name()] = schema
	}

	return schemas
}

func Paths(controller *controllers.RESTController, pathPrefix string) Object {
	idPathParam := Object{
		"in":       "path",
		"name":     controller.IdParamName(),
		"required": true,
		"schema": Object{
			"type": "string",
		},
	}
	return Object{
		pathPrefix + "/" + controller.IdPath(): Object{
			"get": Object{
				"parameters": []Object{
					idPathParam,
				},
				"responses": pathResponses((*controller.ControllerType).Name()),
			},
			"put": Object{
				"parameters": []Object{
					idPathParam,
				},
				"requestBody": Object{
					"$ref": "#/components/requestBodies/" + (*controller.ControllerType).Name(),
				},
				"responses": pathResponses((*controller.ControllerType).Name()),
			},
			"delete": Object{
				"parameters": []Object{
					idPathParam,
				},
				"responses": pathResponses((*controller.ControllerType).Name()),
			},
		},
		pathPrefix: Object{
			"post": Object{
				"requestBody": Object{
					"$ref": "#/components/requestBodies/" + (*controller.ControllerType).Name(),
				},
				"responses": pathResponses((*controller.ControllerType).Name()),
			},
			"get": Object{
				"responses": pathResponses((*controller.ControllerType).Name() + "List"),
			},
		},
	}
}

func pathResponses(typeName string) Object {
	return Object{
		"200": Object{
			"$ref": "#/components/responses/" + typeName,
		},
		"400": Object{
			"$ref": "#/components/responses/BadRequest",
		},
		"401": Object{
			"$ref": "#/components/responses/Unauthorized",
		},
		"403": Object{
			"$ref": "#/components/responses/Forbidden",
		},
		"404": Object{
			"$ref": "#/components/responses/NotFound",
		},
		"500": Object{
			"$ref": "#/components/responses/ServerError",
		},
	}
}

func StructToSchema(reflectType reflect.Type, schemaCache map[reflect.Type]spec.Schema, typePath string) spec.Schema {
	if len(strings.Split(typePath, "/")) > 5 {
		panic(typePath)
	}
	if schema, present := schemaCache[reflectType]; present {
		return schema
	}
	schema := &spec.Schema{}
	AddFieldsToSchema(reflectType, typePath, schema, schemaCache)
	schemaCache[reflectType] = *schema
	return *schema
}

func AddFieldsToSchema(reflectType reflect.Type, typePath string, schema *spec.Schema, schemaCache map[reflect.Type]spec.Schema) {
	for i := 0; i < reflectType.NumField(); i++ {
		field := reflectType.Field(i)

		if tag, present := field.Tag.Lookup("json"); present {
			if tag == "-" {
				continue
			}
		}
		if tag, present := field.Tag.Lookup("openapi"); present {
			if tag == "-" {
				continue
			}
		}
		if field.Anonymous {
			subType := field.Type
			AddFieldsToSchema(subType, typePath, schema, schemaCache)
		}
		schema.SetProperty(
			iris_helpers.CamelToSnakeCase(field.Name),
			FieldToSchema(field, schemaCache, typePath+"/"+reflectType.Name()),
		)
	}
}

func FieldToSchema(field reflect.StructField, schemaCache map[reflect.Type]spec.Schema, typePath string) spec.Schema {
	return TypeToSchema(field.Type, schemaCache, typePath)
}

func TypeToSchema(fieldType reflect.Type, schemaCache map[reflect.Type]spec.Schema, typePath string) spec.Schema {

	if fieldType == reflect.TypeOf(datatypes.JSON{}) {
		return *spec.MapProperty(nil)
	}
	if fieldType == reflect.TypeOf(uuid.UUID{}) {
		return *spec.StringProperty()
	}
	if fieldType == reflect.TypeOf(time.Time{}) {
		return *spec.DateTimeProperty()
	}
	switch fieldType.Kind() {
	case reflect.Struct:
		StructToSchema(fieldType, schemaCache, typePath)
		return *spec.RefSchema("#/components/schemas/" + fieldType.Name())
	case reflect.String:
		return *spec.StringProperty()
	case reflect.Map:
		return *spec.MapProperty(nil)
	case reflect.Array:
	case reflect.Slice:
		arrayType := fieldType.Elem()
		arraySchema := TypeToSchema(arrayType, schemaCache, typePath)
		if arrayType.Kind() == reflect.Struct {
			return *spec.RefSchema("#/components/schemas/" + arrayType.Name())
		}
		return *spec.ArrayProperty(&arraySchema)
	case reflect.Bool:
		return *spec.BoolProperty()
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64, reflect.Uint, reflect.Uint8,
		reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return *spec.Int64Property()
	case reflect.Float32, reflect.Float64:
		return *spec.Float64Property()
	}
	panic("Cannot transform " + fieldType.Name() + " to JSON Schema")
}
