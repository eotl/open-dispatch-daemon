package test

import (
	"bytes"
	"codeberg.org/eotl/open-dispatch-daemon/crypto/cert"
	"codeberg.org/eotl/open-dispatch-daemon/crypto/eddsa"
	"codeberg.org/eotl/open-dispatch-daemon/model"
	"codeberg.org/eotl/open-dispatch-daemon/utils/encoding/bech32"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"time"
)

func main() {
	verifyFile := flag.String("v", "", "A file containing signed data to verify format")
	flag.Parse()
	privKey, _ := eddsa.NewKeypair(rand.Reader)
	expiration := time.Now().AddDate(0, 6, 0).Unix()
	// serialize a RawShipment struct

	d := model.RawShipment{Size: "Large", Weight: "Heavy", Speed: "ASAP"}

	serialized := []byte{}
	enc := json.NewEncoder(bytes.NewBuffer(serialized))
	if err := enc.Encode(&d); err != nil {
		return
	}

	if *verifyFile != "" {
		fmt.Println("Reading JSON off disk:\n")
		verify, err := ioutil.ReadFile(*verifyFile)
		if err != nil {
			panic(err)
		}
		dump(verify)
	} else {
		fmt.Println("Default JSON of struct before signing:\n")
		fmt.Printf("%s", serialized)
		fmt.Println("\n")
		if certificate, err := cert.Sign(privKey, serialized, expiration); err == nil {
			_, err := cert.Verify(privKey.PublicKey(), certificate)
			if err != nil {
				panic(err)
			}
			fmt.Println("Private Key is:\n")
			fmt.Printf("%s\n", base64.StdEncoding.EncodeToString(privKey.Bytes()))
			fmt.Println()
			dump(certificate)
			fmt.Println()
		} else {
			panic(err)
		}
	}
}

func dump(verify []byte) {
	// dump the certificate
	fmt.Println("Signed Content:\n")
	fmt.Printf("%s\n", verify)
	fmt.Println()

	// extract the certified data (payload)
	certified, err := cert.GetCertified(verify)
	if err != nil {
		panic(err)
	}

	fmt.Println("Certified Payload:\n")
	fmt.Printf("%s\n", certified)
	fmt.Println()

	signatures, err := cert.GetSignatures(verify)
	if err != nil {
		panic(err)
	}

	fmt.Println("Payload Signature:\n")
	fmt.Printf("%s\n", base64.StdEncoding.EncodeToString(signatures[0].Payload))
	fmt.Println()

	pubKey := new(eddsa.PublicKey)
	b, err := bech32.DecodeBech32ToBytes(signatures[0].Identity)
	if err != nil {
		panic(err)
	}
	pubKey.FromBytes(b)

	fmt.Println("Public Key is:\n")
	fmt.Printf("%s\n", pubKey)
	fmt.Println()

	certified, err = cert.Verify(pubKey, verify)
	if err != nil {
		panic(err)
	} else {
		fmt.Println("Successfully verified!!!\n")
	}
}
