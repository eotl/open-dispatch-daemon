const messages = {
    start:{
      title: 'Start',
      welcome: 'Willkommen',
      welcomeText: 'Open Dispatch ist eine einfache Möglichkeit, den Versand von einem Ort zum anderen zu koordinieren, idealerweise mit dem Fahrrad oder dem Wind.',
    }
}

export default messages;
