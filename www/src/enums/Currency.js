const currency = {
  eur: {
    iso: 'EUR',
    symbol: '€'
  },
  gbp: {
    iso: 'GBP',
    symbol: '£'
  },
  usd: {
    iso: 'USD',
    symbol: '$'
  }
}

export default currency;
