const weights = {
    light: {
      name: 'Light',
      kg: '5 kg or less'
    },
    average: {
        name: 'Average',
        kg: '5 - 15 kg'
    },
    heavy: {
        name: 'Heavy',
        kg: '15 - 25 kg'
    },
    very: {
        name: 'Very Heavy',
        kg: '25 - 50 kg'
    },
    crazy: {
        name: 'Crazy',
        kg: '50+ kg'
    }
}

export default weights;
