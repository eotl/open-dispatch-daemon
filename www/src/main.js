import Vue from 'vue';
import VueI18n from 'vue-i18n';
import 'bootstrap';

import en from './locales/en';
import de from './locales/de';

import store from './store';
import router from './router';

import App from './App';

Vue.use(VueI18n);

const messages = {
  en,
  de
};

const i18n = new VueI18n({
  locale: 'en', // set locale
  messages // set locale messages
});

Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
  store,
  router,
  i18n,
  el: '#open-dispatch'
});
