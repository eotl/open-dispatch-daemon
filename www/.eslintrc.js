module.exports = {
    env: {
        browser: true,
        es2021: true,
        node: true,
    },
    extends: ["eslint:recommended", "plugin:vue/essential"],
    parser: "vue-eslint-parser",
    parserOptions: {
        ecmaVersion: 12,
    },
    plugins: ["vue"],
    root: true,
    rules: {
        no-empty: "off",
        no-unused-vars: "warn"
    }
}
