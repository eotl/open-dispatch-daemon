Open Dispatch Web UI
====================

A HTML5 web interface for [Open Dispatch](https://codeberg.org/eotl/open-dispatch) written in Vue.js


## Install

You need [Yarn](https://yarnpkg.com) package manager installed.

```
$ cd open-dispatch/
$ yarn install
```

## Developing

To compile and hot-reloads (watch) for development

```
$ yarn serve
```

## Building

To compiles and minifies for production do

```
$ yarn build
```

The files live in `dist/`

## Lints And Fixes

```
$ yarn lint
```
