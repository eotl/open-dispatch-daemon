// shipment.go - open-dispatch storage types.
// Copyright (C) 2020 Masala.
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package model

import (
	"codeberg.org/eotl/open-dispatch-daemon/iris-helpers/ssr"
	"codeberg.org/eotl/open-dispatch-daemon/utils"
	"encoding/json"
	"fmt"
	"github.com/mitchellh/copystructure"
	"gorm.io/datatypes"
	"reflect"
	"time"
)

// RawShipment represents a shipment
type RawShipment struct {
	Size              string           `json:"size"            `
	Weight            string           `json:"weight"          `
	Speed             string           `json:"speed"           `
	ZoneIDPickup      string           `json:"zone_id_pickup"  `
	ZoneIDDestination string           `json:"zone_id_destination"`
	CreatedAt         time.Time        `json:"created_at"  `
	UpdatedAt         time.Time        `json:"updated_at"  `
	Records           []ShipmentRecord `json:"records"`
	Assignment        Assignment       `json:"assignment"`
}

// Shipment represents a shipment and its authorized key
type Shipment struct {
	RawShipment
	ssr.SignData
}

func (shipment Shipment) Id() string {
	return shipment.ID
}

type ShipmentRecord struct {
	ID         string         `json:"id" gorm:"<-:create"`
	ShipmentID string         `json:"shipment_id" gorm:"<-:create"`
	Data       datatypes.JSON `json:"data" gorm:"<-:create"`
	CreatedAt  time.Time      `json:"created_at"  gorm:"<-:create"`
}

func (shipment *Shipment) ConsolidateShipmentData() (map[string]interface{}, error) {

	initialRecord := shipment.Records[0]
	if initialRecord.ID != initialRecord.ShipmentID {
		return nil, fmt.Errorf("Invalid initial record")
	}

	original, err := extractClearRecord(initialRecord)
	if err != nil {
		return nil, err
	}

	history := []map[string]interface{}{}

	for _, record := range shipment.Records[1:] {
		clearRecord, err := extractClearRecord(record)
		if err != nil {
			break
		}
		original = utils.UpdateMap(original, clearRecord["update"].(map[string]interface{}))
		history = append(history, copystructure.Must(copystructure.Copy(clearRecord)).(map[string]interface{}))
	}
	original["updates"] = history
	return original, nil
}

func extractClearRecord(record ShipmentRecord) (map[string]interface{}, error) {
	_, clearRecordBytes, err := ssr.ExtractSignData(record.Data)
	if err != nil {
		return nil, err
	}
	clearRecord := map[string]interface{}{}
	json.Unmarshal(clearRecordBytes, &clearRecord)
	clearRecord["timestamp"] = record.CreatedAt
	return clearRecord, nil
}

var ShipmentType = reflect.TypeOf(Shipment{})
